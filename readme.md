# Magnet Mike

![](v1/img/gif-magnet.gif)

Uncurve your drawing, stick it to the grid.
Very simple to use, nothing much to say.

## Getting Started

This project exist in two versions one -v1- using p5js the other -v2- is coded in python.     

This drawing process has also been adapted in dataface v2, to be applied on font.    


### Prerequisites

#### for v2

the program use these libraries :
* numpy
* svgpathtools
* lxml

## Using magnetmike

### v1

Simply open the index.html in any browser

### v2

Use this command line :
```bash
python3 magnetmike.py myfile.svg --grid 10
```

This code is still a work in progress, mainly on the way it deals with svg files attribute/filtering.    
On ```line 123``` you may have to change :    
```python
svg_path = svg.findall("path")
```

to       

```python
svg_path = svg.findall(".//{http://www.w3.org/2000/svg}path")
```

## To Do


## Author

[Bonjour Monde](http://bonjourmonde.net)

## License

[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)
