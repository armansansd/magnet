
import numpy as np
import math
from svgpathtools import *
import lxml.etree as ET
import os, argparse
parser = argparse.ArgumentParser()

## distance calculator
def calculateDistance(x1,y1,x2,y2):
     dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
     return dist



#### magnetmike
#######
def magnet(paths, gridSize, width, height):
    ## init magnet
    #####
    width = int(width)
    height = int(height)
    grid_points = []
    arr_grid = []
    interval_x = width/gridSize
    interval_y = height/gridSize
    radius = round(math.sqrt(np.square(interval_x)+np.square(interval_y))/2)

    ## define the grid
    #####
    for i in range(0, gridSize):
        for j in range(0, gridSize):
            x = (interval_x)*i+(interval_x)/2;
            y = (interval_y)*j+(interval_y)/2;
            if x <= width and y <= height:
                point = [x,y]
                grid_points.append(point)
    arr_tmp = np.array(grid_points)
    arr_tmp = arr_tmp.astype(float).view(np.complex128)

    for elem in arr_tmp.tolist():
        arr_grid.append(elem[0])

    ########
    ## transform the curves into a serie of line

    new_arr = []
    cut = 80
    for path in paths:
        tmp_path = []
        for seg in path:
            points_array = []
            for i in range(cut):
                point = seg.point(i/(cut-1))
                min_dic = {
                    'dist': 400,
                    'points': '(80+80j)',
                }
                for grid_pt in arr_grid:
                    dist = calculateDistance(grid_pt.real, grid_pt.imag, point.real, point.imag )
                    if dist <= radius:
                        if dist < min_dic['dist'] :
                            min_dic['points'] = grid_pt
                            min_dic['dist'] = dist
                if min_dic['points'] != '(80+80j)':
                    points_array.append(min_dic['points'])

                ## remove duplicate points
                ######
            arr_clean = sorted(set(points_array), key=points_array.index)
            tmp_path.append(arr_clean)

        points_array = []
        for points in tmp_path:
            for point in points:
                points_array.append(point)

        final_points = sorted(set(points_array), key=points_array.index)

            ## redraw the path
            ######
        new_path = Path()
        for idx, point in enumerate(final_points):
            if idx < len(final_points)-1:
                if isinstance(point, complex):
                    line = Line(start=point,end=final_points[idx+1])
                    new_path.append(line)
            if idx == len(final_points)-1:
                if isinstance(point, complex):
                    line = Line(start=point,end=point)
                    new_path.append(line)

        new_arr.append(new_path)

        ## redraw the grid
        ######
        # new_path = Path()
        # for idx, point in enumerate(arr_grid):
        #     if idx < len(arr_grid)-1:
        #         if isinstance(point, complex):
        #             line = Line(start=point,end=arr_grid[idx+1])
        #             new_path.append(line)
        #     if idx == len(arr_grid)-1:
        #         if isinstance(point, complex):
        #             line = Line(start=point,end=point)
        #             new_path.append(line)
        #
        # new_arr.append(new_path)

    return new_arr



def processDrawing( file , grid ):
        print("processing : "+args.file.name)

        base = os.path.splitext(args.file.name)
        numb = str(grid)

        xml = ET.parse(file)
        svg = xml.getroot()
        ## here we should clean the svg to get proper height scal and path
        svg_path = svg.findall("path")
        # print(svg_path)
        width = svg.get("width")
        height = svg.get("height")

        for path in svg_path:
            string_d = path.get("d")
            sp = "M"
            lines =  [sp+e for e in string_d.split(sp) if e]
            drawing_path_arr= []
            for line in lines:
                drawing_path_arr.append(parse_path(line))

            new_path = magnet(drawing_path_arr, grid, width, height)

            ## rebuild
            ####
            d_array =[]
            if  new_path is not None:
                for contour in new_path:
                    if contour != Path() and contour is not None:
                        d_array.append(contour.d())
            #
            # ## reexport a path in svg readable path
            # #####
            path.attrib["d"] = ' '.join(d_array)



            # path.attrib["d"] = new_path[0].d()

        op_name = 'output_'+numb+'_'+base[0]+'.svg';
        f = open(op_name, 'w')
        f.write(ET.tostring(svg, pretty_print=True).decode("utf-8"))
        f.close()

        print("output : "+op_name)


######

parser.add_argument('--grid', help='nb of point per line', type=int)
parser.add_argument('file', type=argparse.FileType('r'))
args = parser.parse_args()

file = args.file.name
grid = args.grid if args.grid is not None else 5

processDrawing(file, grid)

#####
