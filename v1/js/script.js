/*
title : Magnet Mike
date : 06 july 2018
written by : armansansd
email : contact@armansansd.net
based on : p5js and p5js.svg
under GPL license
*/

var w = 500;
var h = 600;
var r = 5;
var i = 0;
var j = 0;
var grid = [];
var path = [];
var newpath, magnet_path, radius;

var svg;

var drawing = new Object();

//function preload() {
 // svg = loadSVG('data/title.svg');
//}

function setup() {
  canvas = createCanvas(w, h);
  strokeWeight(2);
  //image(svg, 0, 0, 50, 50);
  background(204, 204, 204);
  gridy(r);
}

function touchMoved() {
  stroke('black');
	line(mouseX, mouseY, pmouseX, pmouseY);
  var x = mouseX;
  var y = mouseY;
  if( x >= 0 && y >= 0){
    path.push({x,y});
    drawing["path"+i] = path;
    return false;
  }
}
function touchEnded(){
  if( mouseX >= 0 && mouseY >= 0){
    path = [];
    i += 1;
  }
}

function keyPressed() {
  if (keyCode === ENTER) {
    magnet();
    display();
  }
}

function magnet(){
j = 0;
magnet_path = new Object();

for(var path in drawing){
  newpath = [];
  var count = 0;
  drawing[path].forEach(function(path_point){

    var closest = [];
    grid.forEach(function(grid_point){
      var d = int(dist(path_point.x, path_point.y, grid_point.x, grid_point.y));
      if(d <= radius ){

         var x = grid_point.x;
         var y = grid_point.y;

         closest.push({x,y,d});

      }

    });

    closest.sort(function(a, b){
      return a.d-b.d
    });

    newpath.push(closest[0]);
    count += 1;

  });

  
  newpath = newpath.reduce((unique, o) => {
    if(!unique.some(obj => obj.x === o.x && obj.y === o.y)) {
      unique.push(o);
    }
    return unique;
  },[]);

  console.log(newpath);
  magnet_path["path"+j] = newpath;

  j+=1;
}

}

function display(){
  for(var path in magnet_path){
  magnet_path[path].forEach(function(element, index){
    stroke('blue');
    strokeWeight(4);
    if (magnet_path[path][index + 1] != undefined) {
      line(element.x, element.y, magnet_path[path][index + 1].x, magnet_path[path][index + 1].y);
    }
  });
  }
  for(var path in drawing){
  drawing[path].forEach(function(element, index){
    stroke('yellow');
    strokeWeight(1);
    //console.log("drawing!");
    if (drawing[path][index + 1] != undefined) {
      line(element.x, element.y, drawing[path][index + 1].x, drawing[path][index + 1].y);
    }
  });
  }
}


function gridy(n){
  radius = Math.sqrt(Math.pow((w/n),2)+Math.pow((h/n),2))/2
  grid = [];
  background(204, 204, 204);
  stroke("black");
  strokeWeight(1);
  rect(0, 0, w-1, h-1);
  stroke('lightblue');
  strokeWeight(2);
  for(var i = 0; (w/n)*i+2 < w; i ++){
    for(var j = 0; (h/n)*j+2 < h; j ++){
      var x = (w/n)*i+(w/n)/2;
      var y = (h/n)*j+(h/n)/2;
      point(x,y);
      grid.push({x,y});
    }
  }
}
