$(function() {
  var handle = $( "#custom-handle" );
  $( "#slider" ).slider({
    min: 1,
    max: 100,
    create: function() {
      handle.text( $( this ).slider( "value" ) );
    },
    slide: function( event, ui ) {
      handle.text( ui.value );
    },
    change: function(event, ui){
      gridy(ui.value);
      magnet();
      display();
    }
  });
  $( "#button" ).click(function(){
      var myp5 = new p5(s, "svgsandbox");
  });
});
