
var s = function( sketch ) {
  sketch.setup = function() {
    sketch.createCanvas(w, h, SVG);
  };

  sketch.draw = function() {
    sketch.noLoop();
    for(var path in magnet_path){
    magnet_path[path].forEach(function(element, index){
      sketch.stroke('black');
      sketch.strokeWeight(4);
      if (magnet_path[path][index + 1] != undefined) {
        sketch.line(element.x, element.y, magnet_path[path][index + 1].x, magnet_path[path][index + 1].y);
      }
    });
    }
    sketch.save("mydrawing.svg");
  };
};
